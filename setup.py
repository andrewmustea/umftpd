"""Setup module for editable installs."""

import sys

import setuptools
from setuptools import setup

info = {'egg_info', 'dist_info'}
stver = setuptools.__version__
if int(stver.split('.', 1)[0]) < 63 and not info.intersection(sys.argv):
    raise NotImplementedError((
        f'setuptools-{stver} from {setuptools.__path__} is in use,'
        ' but setuptools>=63.0 is required'
        ))

setup(setup_requires=['setuptools>=63.0', 'wheel'])
