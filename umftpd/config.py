"""Config classes."""

import collections.abc
import dataclasses
import datetime
import functools
import getpass
import json
import logging
import os
import pathlib
import sys
import typing

try:
    import importlib_metadata
except ImportError:
    import importlib.metadata as importlib_metadata

import umftpd.crypto as crypto

from gi.repository import GLib

K = typing.TypeVar('K', bound='ConfigBase')
M = typing.TypeVar('M', bound=typing.Mapping[str, typing.Any])
A = typing.TypeVar('A')
R = typing.TypeVar('R')

logger = logging.getLogger(__name__)

UID = os.getuid()

APPCONFIG_DIR = pathlib.Path(GLib.get_user_config_dir(), 'umftpd')
APPCONFIG_FILE = APPCONFIG_DIR / 'config.json'
FLATPAK = pathlib.Path('/.flatpak-info').exists()
FILESYSTEM_ENCODING = sys.getfilesystemencoding()

CWD = pathlib.Path.cwd()
USER_HOME = pathlib.Path.home()
DEFAULT_DIRECTORY = pathlib.Path(GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE,
    ))

metadata = importlib_metadata.metadata(__package__)


class AuthConfig:
    __slots__ = ('config',)

    def __init__(self, config: 'Config') -> None:
        self.config = config

    @property
    def username(self) -> str:
        return self.config.username or self.config.default.username

    @property
    def home(self) -> str:
        return str(self.config.directory)

    def __call__(self, username: str, password: str) -> bool:
        if username != self.username:
            return False
        if self.config.password:
            return self.config.password == password
        if self.config.password_hash:
            return self.config.password_hash.validate(password)
        return self.config.default.auth(username, password)


class ConfigBase:

    def __post_init__(self, _default: bool = False):
        if _default:
            return

        for field, value in self._values():
            parser = getattr(self, f'_parse_{field.name}', None)
            try:
                if callable(parser):
                    self.__dict__[field.name] = parser(value)
                elif isinstance(field.default, ConfigBase):
                    self.__dict__[field.name] = field.default.from_data(value)
            except (TypeError, ValueError, KeyError, IndexError):
                self.__dict__[field.name] = field.default

    @classmethod
    @functools.cache
    def _fields(cls) -> tuple[dataclasses.Field, ...]:
        return dataclasses.fields(cls)

    def _values(self) -> tuple[tuple[dataclasses.Field, typing.Any], ...]:
        for field in self._fields():
            if not field.name.startswith('_'):
                value = getattr(self, field.name)
                if value != field.default:
                    yield field, value

    @property
    def data(self) -> dict[str, typing.Any]:
        return {
            field.name: value.data if isinstance(value, ConfigBase) else value
            for field, value in self._values()
            }

    @classmethod
    def from_data(cls, data: M) -> 'ConfigBase':
        return cls(**{
            field.name: value
            for field, value in (
                (field, data.get(field.name, field.default))
                for field in cls._fields()
                if not field.name.startswith('_')
                )
            if field.default != value
            })


@dataclasses.dataclass(frozen=True)
class Info(ConfigBase):

    last: typing.Optional[datetime.datetime] = None

    @property
    def data(self):
        data = dict(super().data)

        last = data.get('last')
        if last:
            data['last'] = {
                'iso': last.astimezone().isoformat(),
                'unix': last.timestamp(),
                }

        return data

    def _parse_last(self, value):
        if value and not isinstance(value, datetime.datetime):
            ts = (
                value['unix']
                if isinstance(value, collections.abc.Mapping) else
                value
                )
            return datetime.datetime.fromtimestamp(ts).astimezone()
        return value or None


@dataclasses.dataclass(frozen=True)
class Ui(ConfigBase):

    logs: bool = False


@dataclasses.dataclass(frozen=True)
class Config(ConfigBase):

    username: typing.Optional[str] = None
    password: typing.Optional[str] = None
    password_hash: typing.Optional[crypto.CryptoHash] = None
    port: int = 2121 if UID else 21
    secure: int = 1
    directory: pathlib.Path = DEFAULT_DIRECTORY
    readonly: bool = True
    keyfile: pathlib.Path = APPCONFIG_DIR / 'key.pem'
    certfile: pathlib.Path = APPCONFIG_DIR / 'cert.pem'
    ui: Ui = Ui()
    info: Info = Info()

    _default: dataclasses.InitVar[bool] = False

    def __post_init__(self, _default: bool = False) -> None:
        super().__post_init__(_default)

        password, passhash = self.password, self.password_hash
        if password and not (passhash and passhash.validate(password)):
            if passhash:
                logger.warning('Ignoring password hash: missmatch')
            self.__dict__.update(
                password_hash=crypto.CryptoHash.from_text(password),
                )

    def _parse_username(self, value):
        if value != self.default.username:
            return value
        return None

    def _parse_password(self, value):
        if value != self.default.password:
            return value
        return None

    def _parse_password_hash(self, value):
        return (
            crypto.CryptoHash.from_data(value)
            if value and not isinstance(value, crypto.CryptoHash) else
            value or None
            )

    def _parse_directory(self, value) -> pathlib.Path:
        directory = pathlib.Path(value)
        return directory if directory.is_dir() else DEFAULT_DIRECTORY

    def _parse_secure(self, value) -> int:
        return int(value)

    @property
    def default(self) -> 'Config':
        return self.from_defaults()

    @property
    def transport(self) -> str:
        return (None, 'SSL/TLS', 'SSH')[self.secure]

    @property
    def protocols(self) -> str:
        return (
            ('ftp',),
            ('ftp', 'ftps'),
            ('sftp', 'scp'),
            )[self.secure]

    @property
    def ssl(self) -> crypto.SSL:
        return crypto.SSL.from_paths(
            self.keyfile,
            self.certfile,
            self.keyfile.is_relative_to(APPCONFIG_DIR),
            self.certfile.is_relative_to(APPCONFIG_DIR),
            )

    @property
    def auth(self) -> AuthConfig:
        return AuthConfig(self)

    @property
    def data(self) -> dict[str, typing.Any]:
        data = dict(super().data)

        password_hash = data.get('password_hash')
        if password_hash:
            data['password_hash'] = password_hash.data
            data.pop('password', None)

        directory = data.get('directory')
        if directory:
            data['directory'] = str(directory)

        return data

    def save(self) -> None:
        APPCONFIG_FILE.parent.mkdir(parents=True, exist_ok=True)
        APPCONFIG_FILE.write_text(json.dumps(self.data, indent=2))

    def with_updates(self, *args, **kwargs) -> 'Config':
        fixed = {f.name: v for f, v in zip(self._fields(), args)}
        fixed.setdefault('_default', False)

        if any('password' in d for d in (fixed, kwargs)):
            fixed.setdefault('password_hash', None)

        current = self.data
        updates = {**fixed, **kwargs}
        updates.update(
            (k, {**current[k], **updates[k]})
            for k in frozenset(current).intersection(updates)
            if (isinstance(current[k], collections.abc.Mapping)
                and isinstance(updates[k], collections.abc.Mapping))
            )

        return self.from_data({**current, **updates})

    @classmethod
    @functools.cache
    def from_defaults(cls) -> 'Config':
        return cls(
            username=getpass.getuser(),
            password=crypto.generate_human_password(),
            _default=True,
            )

    @classmethod
    def from_environment(cls) -> 'Config':
        try:
            return cls.from_data(json.loads(APPCONFIG_FILE.read_text()))
        except FileNotFoundError:
            pass
        except (ValueError, TypeError):
            logger.warning('Ignoring config file: invalid', exc_info=True)
        return cls()
