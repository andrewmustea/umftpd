"""Network utilities."""

import abc
import contextlib
import functools
import ipaddress
import socket
import sys
import subprocess
import typing

import ifaddr
import zeroconf

import umftpd.config as cfg

from gi.repository import Gio, GLib


MIN_PORT = 1024 if cfg.UID else 1
MAX_PORT = 65535
TIME_WAIT = next((
    int(output.rsplit(sep, 1)[1]) * 2 * unit
    for status, output, sep, unit in (
        (*subprocess.getstatusoutput(cmd), sep, unit)
        for platform, cmd, sep, unit in (
            ('linux', 'sysctl net.ipv4.tcp_fin_timeout', '=', 1),
            ('freebsd', 'sysctl net.inet.tcl.msl', ':', 0.001),
            )
        if sys.platform.startswith(platform)
        )
    if not status
    ), 240)


class Application(abc.ABC):
    config: cfg.Config

    @abc.abstractmethod
    def activate_action(
            self,
            name: str,
            data: typing.Optional[GLib.Variant],
            ) -> None:
        pass


class Server(abc.ABC):

    running: bool

    actions = tuple(
        Gio.SimpleAction(
            name=name,
            parameter_type=vartype and GLib.VariantType(vartype),
            enabled=True,
            )
        for name, vartype in (
            ('server:session', 's'),
            ('server:disconnect', None),
            ('server:upload', 's'),
            ('server:download', 's'),
            )
        )

    @property
    def config(self) -> cfg.Config:
        return self.application.config

    def __init__(self, application: Application, timeout: float) -> None:
        self.application = application
        self.timeout = timeout

    @contextlib.contextmanager
    def publish(self):
        zc = zeroconf.Zeroconf()
        try:
            summary = cfg.metadata['summary']
            for protocol in self.application.config.protocols:
                zc.register_service(zeroconf.ServiceInfo(
                    f'_{protocol}._tcp.local.',
                    f'{summary}._{protocol}._tcp.local.',
                    port=self.application.config.port,
                    server=f'{socket.gethostname()}.local.',
                    ))
            yield
        finally:
            zc.close()

    def emit(self, name, argument=None) -> None:
        self.application.activate_action(
            f'server:{name}',
            argument and GLib.Variant.new_string(argument),
            )

    @abc.abstractmethod
    def stop(self) -> None:
        pass

    @abc.abstractmethod
    def start(self) -> None:
        pass

    @abc.abstractmethod
    def run(self) -> None:
        pass


def check_port(port: int, ipv6: bool = False) -> bool:
    """Check if port is available."""
    family = socket.AF_INET6 if ipv6 else socket.AF_INET
    sock = socket.socket(family, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    with contextlib.closing(sock), contextlib.suppress(OSError):
        sock.bind(('', port))
        return True
    return False


def route_host() -> typing.Iterable[str]:
    """Get network addresses to current host."""
    return [
        str(address)
        for address in (
            (
                (ip.is_IPv4 and ipaddress.IPv4Address(ip.ip))
                or (ip.is_IPv6 and ipaddress.IPv6Address(ip.ip[0]))
                or None
                )
            for adapter in ifaddr.get_adapters()
            for ip in adapter.ips
            )
        if address and not address.is_loopback
        ]


@functools.cache
def time_wait() -> float:
    """Get TCP TIME_WAIT from system configuration, in seconds."""
    for platform, cmd, sep, unit in (
            ('linux', ('sysctl', 'net.ipv4.tcp_fin_timeout'), '=', 2.),
            ('freebsd', ('sysctl', 'net.inet.tcl.msl'), ':', 0.002),
            ):
        if sys.platform.startswith(platform):
            with contextlib.suppress(
                    ValueError,
                    subprocess.CalledProcessError,
                    ):
                _, val = subprocess.check_output(cmd, text=True).rsplit(sep, 1)
                return int(val) * unit
    return 240.
