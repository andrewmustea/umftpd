"""FTP server classes and network utilities."""

import functools
import threading
import typing

import pyftpdlib.authorizers
import pyftpdlib.handlers
import pyftpdlib.servers

import umftpd.config as cfg
import umftpd.server as srv


BANNER = '{}-{} ready.'.format(
    cfg.metadata['name'],
    cfg.metadata['version'],
    )


class Permissions:

    CWD = 'e'
    CDUP = CWD
    LIST = 'l'
    NLST = LIST
    STAT = LIST
    MLSD = LIST
    MLST = LIST
    SIZE = LIST
    RETR = 'r'
    APPE = 'a'
    DELE = 'd'
    RMD = DELE
    RNFR = 'f'
    RNTO = RNFR
    MKD = 'm'
    STOR = 'w'
    STOU = STOR
    SITE_CHMOD = 'M'
    SITE_MFMT = 'T'

    R = CWD + LIST + RETR
    A = APPE + MKD + STOR + SITE_MFMT
    W = A + DELE + RNFR + SITE_CHMOD
    RW = R + W


class Authorizer(pyftpdlib.authorizers.DummyAuthorizer):

    def __init__(self, config: cfg.Config):
        super().__init__()

        self.auth = config.auth
        self.add_user(
            config.auth.username,
            '',
            config.auth.home,
            perm=Permissions.R if config.readonly else Permissions.RW,
            )

    def validate_authentication(self, username, password, handler):
        if username in self.user_table and self.auth(username, password):
            return
        raise pyftpdlib.authorizers.AuthenticationFailed


class FTPHandler(pyftpdlib.handlers.FTPHandler):

    banner = BANNER
    manager: 'FTPServer'
    authorizer: 'Authorizer'

    def _emit(self, name, *args):
        if self.authenticated:
            self.manager.emit(name, *args)

    _partial = functools.partialmethod
    on_disconnect = _partial(_emit, 'disconnect')
    on_login = _partial(_emit, 'session')
    on_file_sent = _partial(_emit, 'download')
    on_file_received = _partial(_emit, 'upload')
    on_incomplete_file_sent = _partial(_emit, 'download')
    on_incomplete_file_received = _partial(_emit, 'upload')


class FTPSHandler(pyftpdlib.handlers.TLS_FTPHandler, FTPHandler):
    pass


class FTPServer(srv.Server):

    backend: pyftpdlib.servers.FTPServer

    @functools.cached_property
    def handler_class(self) -> typing.Type[pyftpdlib.handlers.FTPHandler]:
        base = (FTPHandler, FTPSHandler)[bool(self.config.secure)]
        return type('Handler', (base,), {
            'authorizer': Authorizer(self.config),
            'manager': self,
            'keyfile': str(self.config.ssl.keyfile),
            'certfile': str(self.config.ssl.certfile),
            })

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        address = '', self.config.port
        self.running = True
        self.backend = pyftpdlib.servers.FTPServer(address, self.handler_class)
        self.lock = threading.Lock()

    def start(self) -> None:
        if self.lock.acquire(False):
            self.backend.serve_forever(self.timeout, False, False)
            if self.running:
                self.lock.release()
            else:
                self.backend.close_all()

    def stop(self) -> None:
        if self.lock.acquire(False):
            self.backend.close_all()
        self.running = False

    def run(self) -> None:
        if self.lock.acquire(False):
            with self.publish():
                while self.running:
                    self.backend.ioloop.loop(self.timeout, False)
            self.backend.close_all()
