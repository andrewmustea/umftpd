"""SSH server classes."""

import asyncio
import contextlib
import errno
import functools
import pathlib
import threading
import typing

import asyncssh
import asyncssh.connection

import umftpd.config as cfg
import umftpd.server as srv

supress_stop_iteration = contextlib.suppress(StopIteration)


class SSHServer(asyncssh.SSHServer):

    session = False

    def __init__(self, manager: 'SFTPServer') -> None:
        self.manager = manager

    def validate_host_based_user(self, *_, **__) -> typing.Literal[True]:
        return True

    def password_auth_supported(self) -> typing.Literal[True]:
        return True

    def begin_auth(self, username: str) -> bool:
        return self.manager.config.auth.username == username

    def validate_password(self, username: str, password: str) -> bool:
        if self.manager.config.auth(username, password):
            self.manager.emit('session', username)
            self.session = True
            return True
        return False

    def connection_lost(self, exc: typing.Optional[Exception]) -> None:
        if self.session:
            self.manager.emit('disconnect')


class SFTPHandler(asyncssh.SFTPServer):
    _event_modes = (
        (frozenset('r'), 'download'),
        (frozenset('wacx'), 'upload'),
        )
    _last_emit = None

    def _emit(self, fileobj: typing.BinaryIO) -> None:
        with supress_stop_iteration:
            event = next(
                event
                for mode, event in self._event_modes
                if mode.intersection(fileobj.mode)
                )
            key = event, fileobj.name
            if key != self._last_emit:
                self._last_emit = key
                self.manager.emit(
                    event,
                    pathlib.Path(fileobj.name.decode(cfg.FILESYSTEM_ENCODING))
                    .relative_to(self._chroot.decode(cfg.FILESYSTEM_ENCODING))
                    .as_posix(),
                    )

    def __init__(
            self,
            chan: asyncssh.SSHServerChannel,
            manager: 'SFTPServer',
            ) -> None:
        super().__init__(chan, manager.config.auth.home)
        self.manager = manager

    def close(self, fileobj: typing.BinaryIO) -> None:
        res = super().close(fileobj)
        self._emit(fileobj)
        return res


class ReadOnlySFTPHandler(SFTPHandler):
    empty_attrs = asyncssh.SFTPAttrs()
    allowed_pflags = asyncssh.FXF_READ
    allowed_desired_access = (
        asyncssh.ACE4_READ_DATA
        | asyncssh.ACE4_READ_ATTRIBUTES
        )
    allowed_flags = (
        asyncssh.FXF_OPEN_EXISTING
        | asyncssh.FXF_BLOCK_READ
        | asyncssh.FXF_BLOCK_ADVISORY
        | asyncssh.FXF_NOFOLLOW
        | asyncssh.FXF_ACCESS_AUDIT_ALARM_INFO
        | asyncssh.FXF_ACCESS_BACKUP
        | asyncssh.FXF_BACKUP_STREAM
        )

    def _ro_error(
            self,
            filename: typing.Union[bytes, str, None] = None,
            filename2: typing.Union[bytes, str, None] = None,
            ) -> typing.NoReturn:
        msg = 'Read-only file system'
        raise PermissionError(errno.EROFS, msg, filename, None, filename2)

    def _ro_file(self, file_obj, *_, **__) -> typing.NoReturn:
        self._ro_error(getattr(file_obj, 'name', None))

    def _ro_path(self, path: bytes, *_, **__) -> typing.NoReturn:
        self._ro_error(path)

    def _ro_move(self, oldpath: bytes, newpath: bytes) -> typing.NoReturn:
        self._ro_error(oldpath, newpath)

    def open(
            self,
            path: bytes,
            pflags: int,
            attrs: typing.Any,
            ) -> typing.BinaryIO:
        if pflags & ~self.allowed_pflags:
            self._ro_error(path)
        return super().open(path, pflags, self.empty_attrs)

    def open56(
            self,
            path: bytes,
            desired_access: int,
            flags: int,
            attrs: typing.Any,
            ) -> typing.BinaryIO:
        if desired_access & ~self.allowed_desired_access:
            self._readonly(path)
        flags = flags & self.allowed_flags
        return super().open56(path, desired_access, flags, self.empty_attrs)

    write = _ro_file
    setstat = _ro_path
    fsetstat = _ro_file
    remove = _ro_path
    mkdir = _ro_path
    rmdir = _ro_path
    rename = _ro_move
    symlink = _ro_move
    link = _ro_move
    lock = _ro_file
    unlock = _ro_file
    posix_rename = _ro_move
    fsync = _ro_file


class SFTPServer(srv.Server):

    task: typing.Optional[asyncio.Task[asyncssh.connection.SSHAcceptor]]

    @property
    def running(self):
        return self.loop is not None

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.loop = asyncio.new_event_loop()
        self.event = asyncio.Event()
        self.lock = threading.Lock()

    def start(self) -> None:
        async def server():
            return await asyncssh.listen(
                '',
                self.config.port,
                allow_scp=True,
                reuse_address=True,
                server_factory=functools.partial(SSHServer, manager=self),
                server_host_certs=[str(self.config.ssl.certfile)],
                server_host_keys=[str(self.config.ssl.keyfile)],
                sftp_factory=functools.partial(
                    (
                        ReadOnlySFTPHandler
                        if self.config.readonly else
                        SFTPHandler
                        ),
                    manager=self,
                    ),
                )

        if self.lock.acquire(False):
            self.task = self.loop.create_task(server())
            self.loop.run_until_complete(asyncio.wait_for(
                asyncio.shield(self.task),
                timeout=self.timeout,
                ))
            self.lock.release()

    def cleanup(self) -> None:
        loop, self.loop = self.loop, None
        while tasks := asyncio.all_tasks(loop=loop):
            with contextlib.suppress(asyncio.TimeoutError):
                loop.run_until_complete(asyncio.wait_for(
                    asyncio.gather(*tasks),
                    timeout=self.timeout,
                    ))
        loop.close()

    def stop(self) -> None:
        if self.lock.acquire(False):
            self.cleanup()
        elif self.loop:
            self.loop.call_soon_threadsafe(self.event.set)

    def run(self) -> None:

        async def wait():
            await self.event.wait()
            self.task.cancel()
            connection = await self.task
            connection.close()
            await connection.wait_closed()

        if self.lock.acquire(False):
            with self.publish():
                self.loop.run_until_complete(wait())
            self.cleanup()
