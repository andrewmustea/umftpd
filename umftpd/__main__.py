"""Module script entrypoint."""

import logging
import sys
import typing

import umftpd


def main(
        argv: typing.Optional[typing.Sequence[str]] = None,
        ) -> typing.NoReturn:
    """Run application."""
    logging.basicConfig()
    app = umftpd.Application()
    sys.exit(app.run(sys.argv if argv is None else argv))


if __name__ == '__main__':
    main()
