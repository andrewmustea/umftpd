"""Cryptographic utilities."""

import datetime
import functools
import hashlib
import os
import pathlib
import random
import socket
import typing
import zlib

import cryptography.exceptions
import cryptography.hazmat.primitives.asymmetric.padding as crypto_padding
import cryptography.hazmat.primitives.asymmetric.rsa as crypto_rsa
import cryptography.hazmat.primitives.asymmetric.types as crypto_types
import cryptography.hazmat.primitives.hashes as crypto_hashes
import cryptography.hazmat.primitives.serialization as crypto_serialization
import cryptography.x509
import cryptography.x509.oid as crypto_oid


DEFAULT_HASH_ALGORITHM = 'sha512'

try:
    MACHINE_ID = bytes.fromhex(pathlib.Path('/etc/machine-id').read_text())
except OSError:
    MACHINE_ID = b'w\x85\xfcr\x8b\xbc\xf6U\xaf\xf5gu(\xfc\x1c\xf7'

MACHINE_CRC = zlib.crc32(MACHINE_ID)


class SSL(typing.NamedTuple):

    key: crypto_types.CERTIFICATE_PRIVATE_KEY_TYPES
    keyfile: pathlib.Path
    cert: cryptography.x509.Certificate
    certfile: pathlib.Path

    @classmethod
    @functools.lru_cache(1)
    def from_paths(
            cls,
            keyfile: os.PathLike,
            certfile: os.PathLike,
            recreate_keyfile: bool = False,
            recreate_certfile: bool = False,
            ) -> 'SSL':
        """Get, validate or self-generate certificate files."""
        now = datetime.datetime.utcnow()
        keyfile, certfile = map(pathlib.Path, (keyfile, certfile))
        try:
            key = crypto_serialization.load_pem_private_key(
                keyfile.read_bytes(),
                None,
                )
        except (TypeError, FileNotFoundError):
            if not recreate_keyfile:
                raise

            keyfile.parent.mkdir(parents=True, exist_ok=True)
            key = crypto_rsa.generate_private_key(
                public_exponent=65537,
                key_size=2048,
                )
            keyfile.write_bytes(key.private_bytes(
                encoding=crypto_serialization.Encoding.PEM,
                format=crypto_serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=crypto_serialization.NoEncryption(),
                ))

        try:
            cert = cryptography.x509.load_pem_x509_certificate(
                certfile.read_bytes(),
                )
            if cert.not_valid_after < now:
                raise ValueError
            key.public_key().verify(
                cert.signature,
                cert.tbs_certificate_bytes,
                crypto_padding.PKCS1v15(),
                cert.signature_hash_algorithm,
                )
        except (
                cryptography.exceptions.InvalidKey,
                cryptography.exceptions.InvalidSignature,
                FileNotFoundError,
                ):
            if not recreate_certfile:
                raise

            certfile.parent.mkdir(parents=True, exist_ok=True)
            host = socket.gethostname()
            issuer = cryptography.x509.Name([
                cryptography.x509.NameAttribute(
                    crypto_oid.NameOID.COMMON_NAME,
                    f'{host}-umftpd',
                    ),
                ])
            cert = (
                cryptography.x509.CertificateBuilder()
                .subject_name(issuer)
                .issuer_name(issuer)
                .public_key(key.public_key())
                .serial_number(cryptography.x509.random_serial_number())
                .not_valid_before(now)
                .not_valid_after(now + datetime.timedelta(days=256))
                .add_extension(
                    cryptography.x509.SubjectAlternativeName([
                        cryptography.x509.DNSName(host),
                        cryptography.x509.DNSName('localhost'),
                        ]),
                    critical=False,
                    )
                .sign(key, crypto_hashes.SHA256())
                )
            certfile.write_bytes(cert.public_bytes(
                crypto_serialization.Encoding.PEM,
                ))

        return cls(key, keyfile, cert, certfile)


class CryptoHash(typing.NamedTuple):

    hash_data: bytes
    hash_salt: bytes
    hash_algorithm: str

    @property
    def hash_machine_crc(self):
        return MACHINE_CRC

    @property
    def data(self) -> dict[str, typing.Any]:
        return {
            'hash_data': self.hash_data.hex(),
            'hash_salt': self.hash_salt.hex(),
            'hash_algorithm': self.hash_algorithm,
            'hash_machine_crc': self.hash_machine_crc,
            }

    def validate(self, value: str) -> bool:
        hash_data = generate_hash(
            MACHINE_ID,
            value.encode('utf-8'),
            self.hash_salt,
            )
        return self == type(self)(hash_data, *self[1:])

    @classmethod
    def from_text(cls, value: str) -> 'CryptoHash':
        hash_salt = os.urandom(1024)
        hash_algorithm = DEFAULT_HASH_ALGORITHM
        hash_data = generate_hash(
            MACHINE_ID,
            value.encode('utf-8'),
            hash_salt,
            algorithm=hash_algorithm,
            )
        return cls(hash_data, hash_salt, hash_algorithm)

    @classmethod
    def from_data(cls, data: typing.Mapping[str, typing.Any]) -> 'CryptoHash':
        hash_algorithm = data['hash_algorithm']
        if not callable(getattr(hashlib, hash_algorithm, None)):
            raise ValueError(f'Invalid hash_algoritm {hash_algorithm}')
        if data['hash_machine_crc'] != MACHINE_CRC:
            raise ValueError('Invalid hash_machine_crc')
        return cls(
            bytes.fromhex(data['hash_data']),
            bytes.fromhex(data['hash_salt']),
            hash_algorithm,
            )


def generate_human_password(words: int = 3, size: int = 5) -> str:
    sample = ''.join(map(''.join, zip(*(
        random.sample(group, len(group))
        for group in (
            words * 'aeiouy',
            words * ('qu', *'bcdfghjkmnprstvwxyz'),
            )
        ))))
    partition = len(sample) // words
    res = [
        sample[i + random.randint(0, 1):i + size]
        for i in range(0, len(sample) - 1, partition)
        ]
    return '-'.join(res[:words])


def generate_hash(
        *data: bytes,
        algorithm: str = DEFAULT_HASH_ALGORITHM,
        ) -> bytes:
    return getattr(hashlib, algorithm)(b''.join(data)).digest()
