"""Module script entrypoint."""

import argparse
import importlib
import json
import sys
import typing

import umftpd.uitk as uitk


class NoArgvError(Exception):
    """Exception raised to reject any argument."""


def handle_demo(argv: typing.Sequence[str]) -> int:
    """Show all known application windows."""
    return uitk.UiDemoAplication().run(argv)


def handle_messages(argv: typing.Sequence[str]) -> None:
    """Print out all registered messages as JSON."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--pretty', action='store_true', help='Pretty JSON')
    args = parser.parse_args(argv)
    print(json.dumps(
        uitk.MessageManager._dump_all(),
        indent=2 if args.pretty else None,
        ))


def handle_render(argv: typing.Sequence[str]) -> None:
    """Render and print mustache template from stdin."""
    if len(argv) > 1:
        raise NoArgvError
    print(uitk.MessageManager._render_all(sys.stdin.read()), end='')


def handle_help(argv: typing.Any) -> None:
    """Show this help message and exit."""


def main(
        argv: typing.Optional[typing.Sequence[str]] = None,
        ) -> typing.NoReturn:
    """Run debug CLI commands."""
    commands = {
        name[7:]: func
        for name, func in globals().items()
        if name.startswith('handle_') and callable(func)
        }
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        add_help=False,
        )
    parser.add_argument(
        'command',
        metavar='<command>',
        nargs='?',
        default='help',
        choices=commands,
        help='\n'.join(f'{k}\n  {v.__doc__}' for k, v in commands.items()),
        )
    parser.add_argument(
        'module',
        nargs='*',
        help='Python module to load UI definitions from.',
        )
    parser.add_argument(
        '-h', '--help',
        action='store_true',
        help=handle_help.__doc__,
        )
    args, extra = parser.parse_known_args(argv)

    for module in args.module:
        importlib.import_module(module)

    if args.help and args.command == 'help':
        parser.print_help()
        sys.exit(0)

    if not args.command:
        print('error: <command> argument is required')
        sys.exit(0)

    try:
        sys.exit(commands[args.command](extra) or 0)
    except NoArgvError:
        print(f'error: command {args.command} does not expect arguments')
        sys.exit(1)


if __name__ == '__main__':
    main()
