r"""
mlogic - conditional blocks for mstache
=======================================

Mstache lambda and getter functions implementing Mustache conditional logic
blocks.

This is a fully supported non-standard extension for Mustache, as it can be
implemented in the same way in other languages with Mustache libraries with
a proper lambda implementation (unlike chevron).

This is an implementation example for JavaScript by using an ES6 Proxy:

.. code-block:: js

    const logicOperators = {
      '<': (a, b) => a < b,
      '<=': (a, b) => a <= b,
      '==': (a, b) => a == b,
      '===': (a, b) => a == b,
      '!=': (a, b) => a != b,
      '!==': (a, b) => a !== b,
      '>=': (a, b) => a >= b,
      '>': (a, b) => a > b,
      '|': (a, b) => a | b,
      '^': (a, b) => a ^ b,
      '&': (a, b) => a & b,
    };
    function logicLambdaFactory(operator_name, operator_keys) {
      return function(){
        return (content, render) => {
          let [a, b] = operator_keys.map((key) => {
            if (Reflect.has(this, key)) return this[key];
            try {
              return JSON.parse(key);
            }
            catch(e) {
              return null;
            }
          });
          return (logicOperators[operator_name](a, b) ? render(content) : '');
        };
      };
    }
    function logicProxy(scope) {
      const
        param = '[0-9A-Za-z_.]+',
        operator = Reflect.ownKeys(logicOperators).join('|'),
        re_operators = RegExp(`^(${param})\\s*(${operator})\\s*(${param})$`);
      return new Proxy(scope, {get(scope, name, receiver){
        if (Reflect.has(scope, name)) return scope[name];
        let match = re_operators.exec(name);
        if (match) return logicLambdaFactory(match[2], [match[1], match[3]]);
        return undefined;
      }});
    }
    console.log(Mustache.render(
        '{{#key>1}}key > 1{{/key>1}}' +
        '{{#nested}}{{#key>2}}nested.key > 2{{/key>2}}{{/nested}}',
        logicProxy({key: 2, nested: {key: 3}}),
        ));

"""

import ast
import contextlib
import operator
import re
import typing

import ustache as mstache

D = typing.TypeVar('D')

suppress_value_error = contextlib.suppress(ValueError)
missing = object()
operators = {
    '<': operator.lt,
    '<=': operator.le,
    '==': operator.eq,
    '===': operator.is_,
    '!=': operator.ne,
    '!==': operator.is_not,
    '>=': operator.ge,
    '>': operator.gt,
    '|': operator.or_,
    '^': operator.xor,
    '&': operator.and_,
    }
re_conditional = re.compile(
    r'^({operand})\s*({operator})\s*({operand})$'.format(
        operand='[0-9A-Za-z_.]+',
        operator='|'.join(map(re.escape, operators)),
        ),
    re.VERBOSE,
    )


def literal(value: str, default: D = None) -> typing.Union[D, typing.Any]:
    with suppress_value_error:
        return ast.literal_eval(value)
    return default


def lambda_render(
        scope: typing.Any,
        scopes: typing.Sequence[typing.Any],
        default_lambda_render: mstache.LambdaRenderFunctionConstructor = (
            mstache.default_lambda_render
            ),
        **kwargs,
        ):

    def render(
            content: str, *,
            operator_name: typing.Optional[str] = None,
            operator_keys: typing.Iterable[str] = (),
            ):
        if operator_name and not operators[operator_name](*(
                mstache.default_getter(scope, scopes, key, literal(key))
                for key in operator_keys
                )):
            return ''
        return default_render(content)

    default_render = default_lambda_render(scope, scopes=scopes, **kwargs)
    return render


def getter(
        scope: typing.Any,
        scopes: typing.Sequence[typing.Any],
        key: str,
        default: D = None,
        *,
        default_getter: mstache.PropertyGetter = mstache.default_getter,
        **kwargs,
        ) -> typing.Union[D, typing.Any]:

    def logic_lambda_function(content, render):
        return render(content, operator_name=operator_name, operator_keys=(
            operator_key_a,
            operator_key_b,
            ))

    value = default_getter(scope, scopes, key, missing, **kwargs)
    if value is missing:
        match = re_conditional.match(key)
        if match:
            operator_key_a, operator_name, operator_key_b = match.groups()
            return logic_lambda_function
        return default
    return value
