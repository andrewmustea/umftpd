"""umftpd - Usermode FTP Server."""

import collections
import concurrent.futures
import datetime
import functools
import itertools
import logging
import os
import pathlib
import re
import time
import typing
import urllib.parse

import umftpd.config as cfg
import umftpd.crypto as crypto
import umftpd.server as srv
import umftpd.server.ftp as ftp
import umftpd.server.ssh as ssh
import umftpd.ui as ui
import umftpd.uitk as uitk

from gi.repository import Adw, Gdk, Gio, GLib, Gtk


__author__ = 'Felipe A Hernandez <ergoithz@gmail.com>'


logger = logging.getLogger(__name__)


RE_MARKUP = re.compile('<[^>]+>')


class GLibLogHandler(logging.Handler):
    def __init__(self, callback):
        super().__init__()
        self.callback = callback

    def emit(self, record):
        GLib.idle_add(self.callback, record)


class Application(uitk.UiApplication):

    application_id = 'eu.ithz.umftpd'

    window: typing.Optional[Adw.ApplicationWindow] = None
    window_size_request: typing.Optional[tuple[int, int]] = None

    initialized: bool = False
    port_valid: bool = True
    hosts: tuple[str] = ()
    server: typing.Optional[srv.Server] = None

    server_loop: typing.Optional[concurrent.futures.Future] = None
    server_timeout: float = 0.5
    server_error_flush: float = 0
    server_statuses = 'starting', 'online', 'offline'

    status_time: float = 0

    actions = srv.Server.actions
    protocol_reverse_cycling: bool = False
    protocols = 'ftp', 'ftps', 'sftp'

    @property
    def server_loop_running(self) -> bool:
        return self.server_loop and self.server_loop.running()

    @property
    def server_class(self) -> typing.Type[srv.Server]:
        return ssh.SFTPServer if self.config.secure == 2 else ftp.FTPServer

    @property
    def server_addresses(self) -> tuple[str]:
        username = self.config.auth.username
        port = self.config.port
        return tuple(
            f'{protocol}://{username}@{host}:{port}'
            for protocol in self.config.protocols
            for host in (f'[{h}]' if ':' in h else h for h in self.hosts)
            )

    @property
    def server_type(self) -> str:
        protocol = self.protocols[self.config.secure]
        return ui.main[f'protocol_switch_{protocol}'].get_label()

    @property
    def home(self) -> str:
        directory = self.config.directory
        return (
            f'~/{directory.relative_to(cfg.USER_HOME).as_posix()}'
            if directory.is_relative_to(cfg.USER_HOME) else
            f'./{directory.relative_to(cfg.CWD).as_posix()}'
            if directory.is_relative_to(cfg.CWD) else
            directory.absolute().as_posix()
            )

    @functools.cached_property
    def urls(self) -> typing.Mapping[str, str]:
        return dict(
            item.split(', ', 1)
            for item in cfg.metadata.get_all('Project-URL')
            if ', ' in item
            )

    @functools.cached_property
    def config(self) -> cfg.Config:
        return cfg.Config.from_environment()

    @functools.cached_property
    def user_directories(self) -> tuple[tuple[pathlib.Path, str, str], ...]:
        theme = Gtk.IconTheme.get_for_display(ui.main.root.get_display())
        home = pathlib.Path.home()
        root = pathlib.Path(home.anchor)
        special_dirs = (
            (pathlib.Path(path), key[10:].replace('_', '').lower())
            for key, path in (
                (key, GLib.get_user_special_dir(value))
                for key, value in vars(GLib.UserDirectory).items()
                if key.startswith('DIRECTORY_')
                )
            if path
            )
        return (
            (home, ui.strings.directory_home, 'user-home'),
            (root, ui.strings.directory_computer, 'drive-harddisk'),
            *(
                (path, path.name, next(
                    filter(theme.has_icon, (f'folder-{slug}', f'user-{slug}')),
                    'folder',
                    ))
                for path, slug in special_dirs
                ),
            )

    @ui.app.connect('handle-local-options')
    def on_local_options(
            self,
            app: 'Application',
            options: GLib.VariantDict,
            ) -> typing.Literal[-1]:
        size = options.lookup_value('window-size', GLib.VariantType('s'))
        if size:
            w, h = map(int, size.get_string().split('x'))
            self.window_size_request = w, h
        return -1

    @ui.app.connect('startup')
    def on_startup(self, app: 'Application') -> None:
        self.initialized = True

    @ui.app.connect('activate')
    def on_activate(self, app: 'Application') -> None:
        if self.window_size_request:
            ui.main.root.set_default_size(*self.window_size_request)
        ui.main.root.present()

    @ui.app.connect('shutdown')
    def on_shutdown(self, app: 'Application') -> None:
        if self.server:
            self.server.stop()
        self.executor.shutdown()

    @ui.app.connect(
        'server:session', 'activate', None,
        'server_active_sessions', 1,
        )
    @ui.app.connect(
        'server:disconnect', 'activate', None,
        'server_active_sessions', -1,
        )
    @ui.app.connect(
        'server:upload', 'activate', None,
        'server_total_uploads', 1,
        )
    @ui.app.connect(
        'server:download', 'activate', None,
        'server_total_downloads', 1,
        )
    def on_server_stat(
            self,
            action: Gio.Action, data: GLib.Variant,
            target: str, change: int,
            ) -> None:
        self.server_stats[target] += change
        text = ui.strings.render(target, num=self.server_stats[target])
        ui.main[target].set_label(text)

    @ui.about.connect('close-request')
    @ui.directory_error.connect('response')
    @ui.directory_root_error.connect('response')
    def on_dialog(self, widget: Gtk.Dialog, res: typing.Any = None) -> None:
        widget.hide()

    @ui.directory.connect('response')
    def on_directory(
            self,
            widget: Gtk.FileChooserNative,
            res: Gtk.ResponseType,
            ) -> None:
        if res == Gtk.ResponseType.ACCEPT:
            selected = widget.get_file()
            if not selected:
                # WORKAROUND(flatpak bug): show error dialog
                # https://github.com/flatpak/xdg-desktop-portal/issues/820
                ui.directory_error.root.show()
                return
            path = selected.get_path()
            if cfg.FLATPAK and path == '/':
                # WORKAROUND(flatpak/fuse limitation): show error dialog
                # fuse cannot mount root, so flatpak returns its internal root
                ui.directory_root_error.root.show()
                return
            self.set_directory(path, True)

    @ui.main.connect('help', 'activate')
    def on_help(self, action: Gio.Action, data: None) -> None:
        Gtk.show_uri(self.window, self.urls['help'], Gdk.CURRENT_TIME)

    @ui.main.connect('about', 'activate')
    def on_about(self, action: Gio.Action, data: None) -> None:
        ui.about.root.present()

    @ui.main.connect('user_reset', 'clicked', None, 'username')
    @ui.main.connect('password_reset', 'clicked', None, 'password')
    @ui.main.connect('directory_reset', 'clicked', None, 'directory')
    @ui.main.connect('port_reset', 'clicked', None, 'port')
    def on_reset(self, widget: Gtk.Button, value: str) -> None:
        getattr(self, f'set_{value}')(None, True)

    @ui.main.connect('share_email', 'clicked', None,
                     'mailto:?subject={summary}&body={body}')
    @ui.main.connect('share_telegram', 'clicked', None,
                     'https://t.me/share/url?text={message}&url={address}')
    @ui.main.connect('share_whatsapp', 'clicked', None,
                     'https://api.whatsapp.com/send?text={message}+{address}')
    def on_share(self, widget: Gtk.Button, uri: str) -> None:
        message = ui.strings.render_share_address_message(
            type=self.server_type,
            )
        summary = ui.strings.render_share_address_summary(
            type=self.server_type,
            )
        address = self.server_addresses[0]
        raw = {
            'address': address,
            'message': message,
            'summary': summary,
            'body': f'{address}\n\n{message}'
            }
        context = {k: urllib.parse.quote_plus(v) for k, v in raw.items()}
        Gtk.show_uri(self.window, uri.format(**context), Gdk.CURRENT_TIME)

    @ui.main.connect('directory_button', 'clicked')
    def on_directory_click(self, widget: Gtk.Button) -> None:
        ui.directory.root.show()

    @ui.main.connect('readonly_switch', 'state-set')
    def on_readonly_change(self, widget: Gtk.Switch, value: bool) -> None:
        self.set_readonly(value, self.initialized)

    @ui.main.connect('port_entry', 'value-changed')
    def on_port_change(self, widget: Gtk.SpinButton) -> None:
        self.set_port(widget.get_value_as_int(), self.initialized)

    @ui.main.connect('logs_toggle', 'clicked')
    def on_logs_toggle(self, widget: Gtk.ToggleButton) -> None:
        self.set_logs_visible(widget.get_active(), True)

    @ui.main.connect('address_toggle', 'clicked')
    def on_address_toggle(self, widget: Gtk.ToggleButton) -> None:
        active = widget.get_active()
        widget.set_icon_name(ui.REVEAL_END_ICONS[active])
        ui.main.address_revealer.set_reveal_child(active)

    @ui.main.connect('protocol_switch_ftp', 'clicked', None, 0)
    @ui.main.connect('protocol_switch_ftps', 'clicked', None, 1)
    @ui.main.connect('protocol_switch_sftp', 'clicked', None, 2)
    def on_protocol_change(self, widget: Gtk.ToggleButton, value: int) -> None:
        self.set_protocol(value, True)

    @ui.main.connect('protocol_row', 'activated')
    def on_protocol_click(self, widget: Gtk.Widget) -> None:
        inc = (1, -1)[self.protocol_reverse_cycling]
        self.set_protocol((self.config.secure + inc) % len(self.protocols))

    @ui.main.connect('server_start', 'activated', None, True)
    @ui.main.connect('server_stop', 'clicked', None, False)
    def on_server_click(self, widget: Gtk.Widget, value: bool) -> None:
        self.set_online(value, True)

    def set_username(self, username: str, user: bool = False) -> None:
        entry: Gtk.Entry = ui.main.user_entry
        entry.set_text('')  # TODO(if made active): make all this conditional
        entry.get_first_child().set_placeholder_text((
            self.config.default.username
            if user or not username else
            username
            ))

        if user:
            self.config = self.config.with_updates(username=username)

    def set_password(self, password: typing.Any, user: bool = False) -> None:
        entry: Gtk.PasswordEntry = ui.main.password_entry
        entry.set_text('')  # TODO(if made active): make all this conditional
        entry.get_first_child().set_placeholder_text((
            self.config.default.password
            if user else
            ui.strings._dummy_password
            if isinstance(password, crypto.CryptoHash) else
            password
            ))

        if user:
            self.config = self.config.with_updates(password=password)

    def set_port(self, port: typing.Optional[int], user: bool = False) -> None:
        entry: Gtk.SpinButton = ui.main.port_entry

        if port is None:
            port = self.config.default.port

        if port != entry.get_value_as_int():
            entry.set_value(port)
            if user:
                return  # wait for value-changed

        if self.check_port(port) and user:
            self.config = self.config.with_updates(port=port)

    def set_directory(
            self,
            path: typing.Optional[os.PathLike],
            user: bool = False,
            ) -> None:
        path = pathlib.Path(path) if path else self.config.default.directory
        icon, label = next(
            (
                (c, l)
                for p, l, c in self.user_directories
                if p.is_dir() and p.samefile(path)
                ),
            ('folder', path.name)
            )

        if user:
            self.config = self.config.with_updates(directory=path)

        ui.main.directory_content.set_icon_name(icon)
        ui.main.directory_content.set_label(label)
        ui.main.directory_button.set_tooltip_markup(
            ui.strings._render_directory_tooltip(home=self.home),
            )

    def set_protocol(self, value: int, user: bool = False) -> None:
        protocol = self.protocols[value]
        button: Gtk.ToggleButton = ui.main[f'protocol_switch_{protocol}']
        if not button.get_active():
            button.set_active(True)
            if user:
                return  # wait for value-changed

        if value in (0, len(self.protocols) - 1):
            self.protocol_reverse_cycling = bool(value)

        for i, proto in enumerate(self.protocols):
            ui.main[f'protocol_{proto}_label'].set_visible(i == value)

        if self.config.secure != value:
            self.config = self.config.with_updates(secure=value)

    def set_readonly(self, readonly: bool, user: bool = False) -> None:
        switch: Gtk.Switch = ui.main.readonly_switch

        if readonly != switch.get_active():
            switch.set_active(readonly)
            if user:
                return  # wait for value-changed

        ui.main.readonly_disabled.set_reveal_child(not readonly)

        if user:
            self.config = self.config.with_updates(readonly=readonly)

    def set_logs_visible(self, state: bool, user: bool = False):
        revealer = ui.main.logs_revealer
        toggle = ui.main.logs_toggle
        page = ui.main.status_page
        tooltips = ui.strings.server_logs_show, ui.strings.server_logs_hide

        if state != toggle.get_active():
            toggle.set_active(state)

        page.set_vexpand(not state)
        revealer.set_visible(state)
        toggle.set_icon_name(ui.REVEAL_UP_ICONS[state])
        toggle.set_tooltip_text(tooltips[state])

        if state:
            GLib.idle_add(revealer.set_reveal_child, True)
            GLib.idle_add(self.scroll_logs_to_bottom)
        else:
            revealer.set_reveal_child(False)

        if user:
            self.set_config(ui={'logs': state})

    def set_server_error(self, error: typing.Optional[str] = None) -> None:
        row = ui.main.server_start
        revealer = ui.main.server_error_revealer

        if error:
            self.server_error_flush = time.time() + 1 + (
                revealer.get_transition_duration() / 1000.
                )
            ui.main.server_error_label.set_label(error)

        (row.add_css_class if error else row.remove_css_class)('error')
        revealer.set_reveal_child(error is not None)

    def set_online(self, state: bool, user: bool = False):
        if state:
            self.hosts = srv.route_host()
            if not self.hosts:
                self.set_server_error(ui.strings.server_error_no_host)
                logger.warning('Server start bounced, no route to host')
                return

            if self.server_loop_running:
                self.set_server_error(ui.strings.server_error_running)
                logger.warning('Server start bounced, already running')
                return

            if not (self.port_valid and self.check_port(self.config.port)):
                ui.main.port_entry.grab_focus()
                logger.warning('Server start bounced, invalid port')
                return

            self.set_status('starting')
            self.set_server_error()

        pages = ui.main.page_left, ui.main.page_right
        transitions = (
            Gtk.StackTransitionType.SLIDE_RIGHT,
            Gtk.StackTransitionType.SLIDE_LEFT,
            )

        ui.main.back_revealer.set_reveal_child(state)
        ui.main.stack.set_transition_type(transitions[state])
        ui.main.stack.set_visible_child(pages[state])

        if not state:
            if self.server:
                self.server.stop()
            return

        ui.main.server_type.set_label(ui.strings.render_server_status_type(
            type=self.server_type,
            ))

        for target in self.server_stats:
            ui.main[target].set_label(ui.strings.render(target, num=0))
        self.server_stats.clear()

        self.set_config(**{
            key: value
            for key, value in (
                # update values from passive fields
                ('username', ui.main.user_entry.get_text()),
                ('password', ui.main.password_entry.get_text()),
                )
            if value
            })

        addresses = self.server_addresses
        addrbox = ui.main.address_box
        ui.main.address_label.set_label(addresses[0])
        ui.main.address_toggle.set_visible(len(addresses) > 1)
        ui.main.address_revealer.set_visible(len(addresses) > 1)
        for address, widget in itertools.zip_longest(
                addresses[1:],
                tuple(addrbox),
                ):
            if address and widget:
                widget.set_label(address)
            elif address:
                addrbox.append(ui.UiAddressLabel(label=address))
            else:
                addrbox.remove(widget)

        ui.main.status_page.set_icon_name(ui.STATUS_ICON_DISCONNECTED)

        self.server_loop = self.executor.submit(self.loop_server)

    def set_status(self, status: str, created=None):
        if created and created < self.status_time:
            return
        self.status_time = time.time()

        for name in self.server_statuses:
            ui.main[f'server_status_{name}'].set_visible(name == status)

    def set_config(self, **updates) -> cfg.Config:
        self.config = config = self.config.with_updates(
            info={**updates.pop('info', {}), 'last': datetime.datetime.now()},
            **updates,
            )
        config.save()
        return config

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, application_id=self.application_id, **kwargs)

        self.get_style_manager().set_color_scheme(Adw.ColorScheme.PREFER_DARK)
        self.set_accels_for_action('app.help', ['F1'])
        self.add_main_option(
            'window-size',
            ord('s'),  # WTF GObject!
            0,
            GLib.OptionArg.STRING,
            'Desired window size',
            'WIDTHxHEIGHT',
            )

        self.server_stats = collections.defaultdict(int)

        self.executor = concurrent.futures.ThreadPoolExecutor()

        self.textbuffer: Gtk.TextBuffer = ui.main.textview.get_buffer()
        self.textend = self.textbuffer.get_end_iter()
        self.textmark = self.textbuffer.create_mark('', self.textend, False)

        ui.main.port_entry.set_range(srv.MIN_PORT, srv.MAX_PORT)

        self.set_username(self.config.username)
        self.set_password((
            self.config.password
            or self.config.password_hash
            or self.config.default.password
            ))
        self.set_directory(self.config.directory)
        self.set_readonly(self.config.readonly)
        self.set_port(self.config.port)
        self.set_protocol(self.config.secure)
        self.set_logs_visible(self.config.ui.logs)

        handler = GLibLogHandler(self.on_log_record)

        for lib_name in ('pyftpdlib', 'asyncssh'):
            lib_logger = logging.getLogger(lib_name)
            lib_logger.addHandler(handler)
            lib_logger.setLevel(logging.INFO)

        self.logger = logging.getLogger(f'{__name__}:application')
        self.logger.addHandler(handler)
        self.logger.setLevel(logging.INFO)

    def on_log_record(self, record: logging.LogRecord) -> None:
        created = datetime.datetime.fromtimestamp(record.created)
        message = '<tt>{} {} </tt>{}\n'.format(
            created.replace(microsecond=0).isoformat(' '),
            record.levelname,
            (
                getattr(record, 'markup', None)
                or f'<b>{GLib.markup_escape_text(record.getMessage())}</b>'
                ),
            )

        textview: Gtk.TextView = ui.main.textview
        adjustment: Gtk.Adjustment = textview.get_vadjustment()
        bottomed = getattr(record, 'scroll', None) or 1 >= (
            adjustment.get_upper()
            - adjustment.get_value()
            - adjustment.get_page_size()
            - textview.get_top_margin()
            - textview.get_bottom_margin()
            )
        self.textbuffer.insert_markup(self.textend, message, -1)

        if bottomed:
            GLib.idle_add(self.scroll_logs_to_bottom)

    def scroll_logs_to_bottom(self):
        ui.main.textview.scroll_to_mark(self.textmark, 0, False, 0, 0)

    def check_port(self, port: int) -> bool:
        check = functools.partial(srv.check_port, port)
        self.port_valid = valid = all(self.executor.map(check, (True, False)))

        box: Gtk.Box = ui.main.port_box
        (box.remove_css_class if valid else box.add_css_class)('error')

        if not valid:
            ui.main.port_error.set_label(ui.strings.port_unavailable_warning)

        ui.main.port_error_revealer.set_reveal_child(not valid)
        return valid

    def loop_server(self) -> None:
        try:
            self.server = server = self.server_class(self, self.server_timeout)
            server.start()

            message = ui.strings.render_server_log_info(
                config=self.config,
                home=self.home,
                addresses=self.server_addresses,
                )
            self.logger.info(
                RE_MARKUP.sub('', message),
                extra={'markup': message, 'scroll': True},
                )
            GLib.idle_add(self.set_status, 'online')
            GLib.idle_add(
                ui.main.status_page.set_icon_name,
                ui.STATUS_ICON_CONNECTED,
                )
            server.run()
        except Exception as e:
            self.logger.exception(e)
        finally:
            GLib.timeout_add(
                int(max(0, self.server_error_flush - time.time()) * 1000),
                self.set_server_error,
                )
            GLib.idle_add(self.set_status, 'offline')
            self.logger.info(ui.strings.server_log_stopped)
